	org $8000

sdata equ $db00 ; this is where the data needs loaded

	call decode_header
	call tile2udg
	jp udgmap2disp


tile_map dw 0 ;    pointer to the tile map
tile_store dw 0 ; pointer to the tile store
udg_store dw 0 ; pointer to the udg store

decode_header

	ld de,sdata
	ld ix,tile_map
	ld hl,7  ; size of header
	add hl,de
	ld (ix+0),l
	ld (ix+1),h ; tile_map located
	ld hl,47
	add hl,de
	ld (ix+4),l
	ld (ix+5),h  ; udg_store
	push hl ; save it for later - header already added!
	ld hl,5
	add hl,de
	ld l,(hl) ; l = num_udg
	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	ex de,hl
	pop hl
	add hl,de
	ld (ix+2),l
	ld (ix+3),h  ; tile_store
	ret

tile2udg

	exx
	push de
	push hl
	exx

	ld bc,$0805
	ld hl,(tile_map)
	exx
	ld hl,udg_map
	ld de,attrib_map
	exx
tloopx  push bc
	push hl
	exx
	push de
	push hl
	exx
tloopy  push bc
	push hl
	ld l,(hl) ; this is the tile
	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	ld de,(tile_store)
	add hl,de

;hl points to the tile we want

	exx
	push hl
	pop ix
	exx
		; ix points to udg_map

	ld b,$04
dloop
	ld a,(hl)
	ld (ix+0),a
	inc hl
	ld a,(hl)
	ld (ix+32),a
	inc hl
	ld a,(hl)
	ld (ix+64),a
	inc hl
	ld a,(hl)
	ld (ix+96),a
	inc hl
	inc ix
	djnz dloop

	exx
	push de
	pop ix
	exx
		; ix points to attrib_map

	ld b,4
aloop
	ld a,(hl)
	ld (ix+0),a
	inc hl
	ld a,(hl)
	ld (ix+32),a
	inc hl
	ld a,(hl)
	ld (ix+64),a
	inc hl
	ld a,(hl)
	ld (ix+96),a
	inc hl
	inc ix
	djnz aloop
	
	exx
	ld bc,128
	ex de,hl
	add hl,bc
	ex de,hl
	add hl,bc
	exx
	
	ld de,8
	pop hl
	add hl,de
	pop bc
	dec c
	jr nz, tloopy

	exx
	ld bc,4
	pop hl
	pop de
	ex de,hl
	add hl,bc
	ex de,hl
	add hl,bc
	exx

	pop hl
	inc hl
	pop bc
	djnz tloopx

	exx
	pop hl
	pop de
	exx


	ret


 


; the decoded screen byte stores...


udg_map
	ds 32 * 20 , 0

attrib_map
	ds 32 * 20 , 0



; call to rebuild the screen from the udg_map and the attrib_maps

udgmap2disp
		

		ld hl,attrib_map
		ld de,$5840
		ld bc,640
		ldir

		ld hl, udg_map

udg2disp
		di
		ld de,$4040
		ld b,192 ; 32 * 6
char_loop1	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop1
		
		ld de,$4800
		ld b,0 ; 32 * 8
char_loop2	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop2

		ld de,$5000
		ld b,192 ; 32 *4
char_loop3	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop3

		ei
		ret

print		push hl
		ld h,0
		ld l,a
		add hl,hl
		add hl,hl
		add hl,hl
		ld bc,(udg_store)
		add hl,bc
		ld (stack_restore+1), sp
		ld sp,hl 
		ex de,hl
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		ex de,hl
stack_restore		ld sp, 0 ; SMC
			pop hl
			ret


next db 0

